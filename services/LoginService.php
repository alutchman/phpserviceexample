<?php

class LoginService  extends BaseService {
	public $errorMessage = "";
	public $user = "";
	
	public function show(){
		$this->renderView(__FUNCTION__, 'loginTemplate');
	}
	
	public function validate(){		
		if (isset($this->post['username']) && isset($this->post['password'])  && isset($this->post['captcha']) ) {
			$this->user = $this->post['username'];
			
			$captchaSession = $this->session['digit'];
			$captchaPost = strtoupper($this->post['captcha']); 
			$capchaOK = false;

			$this->error_message = "";
			if($captchaSession === md5(trim($captchaPost)))  {
				$capchaOK = true;
			}		
		    if (!$capchaOK) {
				$this->errorMessage = "Ongeldige Captcha.";
			} else {
				if (($this->post['username'] == 'admin')  && ($this->post['password'] == 'Welkom01')){
					$_SESSION['user_id'] = 'admin';
					$this->redirect('startup','show');
					return;
				} else {
					$this->errorMessage = "Gebruiker onbekend of <br /> ongeldig wachtwoord.";
				}			
			}
		}		
		$this->renderView('show', 'loginTemplate');
		
	}
	
	
	public function logout(){
		session_unset();
		session_destroy();
		$_SESSION = array();
		$this->redirect('login', 'show');
	}
	
}
