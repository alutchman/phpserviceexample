<?php

class ErrorService  extends BaseService {
	public $errorMessage = "";
	public $errorObject;
	
	
	function __construct($errorObject){
		parent::__construct('error');
		$this->errorObject = $errorObject;
	}
		 
	public function show(){
		$this->renderView('message', 'errorTemplate');
	}
	


	
}
