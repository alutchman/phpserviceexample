<?php

abstract class BaseService {
	const     VIEW_ROOT = 'views/';
	protected $post ;
	protected $view ;
	protected $head ;   
	protected $request = array();
	protected $session = array();
	protected $basedir;
	protected $globals;
	protected $mobile;
	protected $jsMobile;
	
	public abstract function show();
	
	
	function __construct($servicePart){
		global $GLOBALS;
		
		$this->globals = $GLOBALS;
		
		$this->basedir = $servicePart;
		$this->request = isset($_GET)     ? $_GET     : array();  
		$this->session = isset($_SESSION) ? $_SESSION : array();
		$this->post    = isset($_POST)    ? $_POST    : array();
		$this->head    = getallheaders();
		
		$this->mobile = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
		$this->jsMobile = $this->mobile ? "true" :"false";	 
	}
		
	
	public function renderView($view, $template = null) {		
		$viewBase = $this->globals['root'].self::VIEW_ROOT;
		$pathToView = $this->file_build_path($viewBase,$this->basedir)."/$view.phtml";
		
		header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache');		
		
		if ($template != null) {
			$pathToTemplate = $this->file_build_path($viewBase)."/$template.phtml";
			include($pathToTemplate);	
		} else {
			include($pathToView);		
		}

			
	}
	
	public function includeScript($phpScript) {
		return $this->globals['root'].$phpScript;
	}
	
	public function printAction($action){	
		echo($this->globals['admin'].DIRECTORY_SEPARATOR.$this->basedir.DIRECTORY_SEPARATOR.$action);
	}
	
	protected function redirect($service, $action){
		$newUrl = $this->globals['admin'].DIRECTORY_SEPARATOR.$service.DIRECTORY_SEPARATOR.$action;
		header("Location: $newUrl");
	}
	
	private function file_build_path(...$segments) {
		return join(DIRECTORY_SEPARATOR, $segments);
	}
		
}
