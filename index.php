<?php
	session_start();
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	mysqli_report(MYSQLI_REPORT_STRICT);
	
    
    $GLOBALS['admin'] = dirname($_SERVER['PHP_SELF']);    
    $GLOBALS['root']  = $_SERVER['DOCUMENT_ROOT'].$GLOBALS['admin'].'/';
    $servicePart      = 'startup';
    $action           = 'show';


	$paths0 =  explode('?',substr($_SERVER['REQUEST_URI'],1+strlen( $GLOBALS['admin'] )));
    $paths1 =  explode('/',$paths0[0]);

    
    
    if ( !isset( $_SESSION['user_id'] ) ) {
		 $servicePart = 'login';
		 if (count($paths1) >= 2) {			
			$action = $paths1[1];
		 }		
	}  else {
		
		if (count($paths1) >= 2) {
			$servicePart = $paths1[0];			
			if ( strlen(trim($paths1[1])) > 0) {			
				$action = $paths1[1];
			}
		}	else {			
			if (count($paths1) == 1 && strlen(trim($paths1[0])) > 0) {			
				$servicePart =  $paths1[0].trim();
			}		
		}	
	}

	$service = ucfirst($servicePart).'Service';

	function autoloadFunction($class) {
 		if (preg_match('/Service$/', $class))
			require_once($GLOBALS['root'] .'services/' . $class . ".php");
		else
			require_once($GLOBALS['root'] .'includes/' . $class . ".php");
	}
	
	spl_autoload_register("autoloadFunction");
	
	
	try {
		$service = new $service($servicePart);
		$service->$action();
	} catch (Exception $e) {
		$serviceX = new ErrorService($e);	
		$serviceX->show();		
	} catch (Error $error) {
        $extendedError = new ExtendedError($error);
        $errstr  = $extendedError->message;			
		$serviceX = new ErrorService($extendedError);	
		$serviceX->show();				
	}


