<?php
  session_start();
  // Adapted for The Art of Web: www.the-art-of-web.com
  // Please acknowledge use of this code by including this header.

  // initialise image with dimensions of 120 x 30 pixels
  //$image = @imagecreatetruecolor(120, 30) or die("Cannot Initialize new GD image stream");
  $width = 100;
  $height = 25;
  $image = @imagecreatetruecolor( $width, $height) or die("Cannot Initialize new GD image stream");

  // set background to white and allocate drawing colours
  $background = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
  imagefill($image, 0, 0, $background);
  $linecolor = imagecolorallocate($image, 0xCC, 0xCC, 0xCC);
  $textcolor = imagecolorallocate($image, 0x33, 0x33, 0x88);

  // draw random lines on canvas 
  for($i=0; $i < 6; $i++) {
    imagesetthickness($image, rand(1,3));
    imageline($image, 0, rand(0,$height), $width , rand(0,$height), $linecolor);
  }

  // add random digits to canvas
  $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ23456789';
  $len = strlen($letters);
  
  
  $digit = '';
  for($x = 5; $x <= 96; $x += 12) {
	$letter = $letters[rand(0, $len-1)];
    $digit .= $letter;
    $fonsize = 5; //max min =1
    $y = rand(0, $height/2-1);
    imagechar($image, $fonsize , $x, $y, $letter, $textcolor);
  }

  // record digits in session variable
  $_SESSION['digit'] = md5($digit);

  // display image and clean up
  header('Content-type: image/png');
  imagepng($image);
  imagedestroy($image);
