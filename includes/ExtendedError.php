<?php

class ExtendedError extends Error {
	public $message;
	public $file;
	public $line;	
	
	function __construct(Error $org) {
		$this->message = $org->message;
		$file = $org->file;
		
		
		$pos = strpos($file, '/httpdocs' );
		
		$this->file = substr($file, $pos);
		$this->line = $org->line;
	}	
}
